# JavaScript



[TOC]


+++

#### Introduction

+ invented by Brendan Eich in 1995
+ Language of the Web (HTML & CSS & JS)
+ ECMAScript
+ dynamic typing

```javascript
var a = 12;
typeof(a); //number
a = "Hello";
typeof(a); //String
```

+++

#### Functions

+ no return type
+ parameters are not mandatory
+ no overloading
+ assign a function to a variable
+ variable scope

```javascript
function Plus(p1, p2){
	var erg = p1 + p2;
  	return erg;
}

var y = Plus(3,4); //7
var x = function(p1, p2){
    return p1 + p2;
}

typeof(x); //function
var z = x(3,2); //5
```
+++

#### Object Oriented Programming

+ no class concept
+ constructors and prototypes

```javascript
function Car(name, cost){
    this.name = name;
  	this.cost = cost;
  	//adding a property inside the constructor:
  	this.owner = "me";
  	this.print = function(){
        document.write("name: " + this.name + ", costs: " + this.cost);
    }
}

//adding a property outside the constructor
Car.prototype.location = "unknown";
var c1 = new Car("Opel", 10000);
var c2 = new Car("Renault", 5000);
```

+ prototypal inheritance

```javascript
function Car(name, cost){
    this.name = name;
  	this.cost = cost;
  	this.type = "vehicle"
}

function ClassicCar(name, cost, age){
    this.constructor(name, cost);
  	this.age = age;
}

ClassicCar.prototype = new Car(); //prototypal inheritance
var o1 = new ClassicCar("Porsche", 60000, 25);
console.log(o1.type); //"vehicle"
console.log(o1.__proto__); //Object{name:undefined, cost: undefined, age: undefined, type="vehicle}
```

+ ES6: class concept

```javascript
class Car{
    constructor(name, cost){
        this.name = name;
        this.cost = cost;
    }
}

class ClassicCar extends Car{
    
}
var d = new ClassicCar("Mercedes", 30000);
```

+++

#### JSON

+ JavaScript Object Notation
+ data format user for data interchange between browser and server storing data
+ easy to convert: 
  + JSON.parse(text)
  + JSON.stringify(object)

```json
{"cars":[
  {"name" : "Mercedes", "cost": 4000},
  {"name" : "Audi", "cost": 5000},
  {"name" : "BMW", "cost": 3000},
]}
```

```html
<p id="car"></p>
<script>
	var obj = JSON.parse(text); //where is this text coming from???
  	document.getElementById("car").innerHTML = obj.cars[1].name + obj.cars[1].cost;
</script>
```
+++
#### How to use JavaScript

+ JS accesses all HTML elements through the DOM
+ JS can change, add or delete HTML elements

```html
<!-- Insert a js script file-->
<!DOCTYPE html>
<html>
	<head>
	</head>
	<body>
  		<script src = "js/main.js"></script>
	</body>
</html>
```

```html
<!-- Insert the js code directly-->
<!DOCTYPE html>
<html>
	<head>
	</head>
	<body>
      	<p id="demo"></p>
  		<script>
          	document.getElementById("demo").innerHTML = "Hello World";
      	</script>
	</body>
</html>
```
+++
